<?php

namespace Dbsync\Service;

use Dbsync\Service\Strategy;
use Pckg\Reflect;
use PDO;

class Sync
{

    public $left, $right;

    protected $tables, $rows, $conflicts;

    protected $sqls = [];

    protected $tableStrategies = [
        'doubloons' => Strategy\Table\Doubloons::class,
        'full' => Strategy\Table\Full::class,
        'missing' => Strategy\Table\Missing::class,
        'overwrite' => Strategy\Table\Overwrite::class,
        'selective' => Strategy\Table\Selective::class,
    ];

    protected $rowStrategies = [
        'conflict' => Strategy\Row\Conflict::class,
        'delete' => Strategy\Row\Delete::class,
        'leave' => Strategy\Row\Leave::class,
        'overwrite' => Strategy\Row\Overwrite::class,
    ];

    public function __construct(PDO $left, PDO $right, $tables, $rows, $conflicts)
    {
        $this->left = $left;
        $this->right = $right;

        $this->tables = $tables;
        $this->rows = $rows;
        $this->conflicts = $conflicts;
    }

    public function buildSql()
    {
        foreach ($this->tables as $table => $tableStrategy) {
            $this->syncTable($table, $tableStrategy);
        }

        return implode("\n\n", $this->sqls);
    }

    protected function syncTable($table, $tableStrategy)
    {
        if (isset($this->rows[$table])) {
            foreach ($this->rows[$table] as $id => $rowStrategy) {
                $this->syncRow($table, $id, $rowStrategy);
            }
        } else {
            Reflect::create($this->tableStrategies[$tableStrategy], [
                'table' => $table,
                $this,
            ])->build();
        }
    }

    protected function syncRow($table, $id, $rowStrategy)
    {
        Reflect::create($this->rowStrategies[$rowStrategy], [
            'id' => $id,
            'table' => $table,
            $this,
        ])->build();
    }

    public function addSql($sql)
    {
        if ($sql) {
            if (substr($sql, strlen($sql) - 1) != ';') {
                $sql .= ';';
            }
            $this->sqls[] = $sql;
        }

        return $this;
    }

}