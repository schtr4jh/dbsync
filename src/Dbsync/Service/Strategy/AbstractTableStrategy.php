<?php

namespace Dbsync\Service\Strategy;

use Dbsync\Service\Sync;

class AbstractTableStrategy implements StrategyInterface
{

    protected $table;

    protected $sync;

    public function __construct($table, Sync $sync)
    {
        $this->sync = $sync;
        $this->table = $table;
    }

    public function build()
    {

    }

}