<?php

namespace Dbsync\Service\Strategy\Row;

use Dbsync\Service\GetRowIdByTable;
use Dbsync\Service\Strategy\AbstractRowStrategy;
use Pckg\Database\Query\Insert;
use Pckg\Database\Query\Update;

class Overwrite extends AbstractRowStrategy
{

    use GetRowIdByTable;

    public function build()
    {
        $ids = $this->getRowIdsByTable($this->table);
        $where = [];
        $idValues = explode('_', $this->id);
        foreach ($ids as $i => $id) {
            if ($id != 'id' && substr($id, -3) != '_id') {
                $id .= '_id';
            }
            $where[] = '`' . $id . '` = \'' . $idValues[$i] . '\'';
        }

        $sql = 'SELECT * FROM `' . $this->table . '` WHERE ' . implode(' AND ', $where);

        $query = $this->sync->left->query($sql);
        $result = $query->execute();
        $rLeft = $query->fetch();

        $query = $this->sync->right->query($sql);
        $result = $query->execute();
        $rRight = $query->fetch();

        if ($rRight) {
            $update = new Update();
            $update->setTable($this->table);
            $update->setSet((array)$rLeft);
            $update->setWhere($where);
            $sql = $update->buildSQL();
        } else {
            $insert = new Insert();
            $insert->setTable($this->table);
            $insert->setInsert((array)$rLeft);
            $sql = $insert->buildSQL();
        }

        $this->sync->addSql($sql);
    }

}