<?php

namespace Dbsync\Service\Strategy\Row;

use Dbsync\Service\GetRowIdByTable;
use Dbsync\Service\Strategy\AbstractRowStrategy;
use Pckg\Database\Query\Insert;

class Conflict extends AbstractRowStrategy
{

    use GetRowIdByTable;

    public function build()
    {
        $ids = $this->getRowIdsByTable($this->table);
        $where = [];
        $idValues = explode('_', $this->id);
        foreach ($ids as $i => $id) {
            $where[] = '`' . $id . '` = \'' . $idValues[$i] . '\'';
        }

        $sql = 'SELECT * FROM `' . $this->table . '`';

        $query = $this->sync->left->query($sql);
        $result = $query->execute();
        $rLeft = $query->fetch();

        $hasId = isset($rLeft->id);
        unset($rLeft->id);

        $insert = new Insert();
        $insert->setTable($this->table);
        $insert->setInsert((array)$rLeft);
        $sql = $insert->buildSQL();

        if (!$hasId) {
            $sql = '/* ' . $sql . ' */';
        }

        $this->sync->addSql($sql);
    }

}