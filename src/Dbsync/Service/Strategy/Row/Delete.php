<?php

namespace Dbsync\Service\Strategy\Row;

use Dbsync\Service\GetRowIdByTable;
use Dbsync\Service\Strategy\AbstractRowStrategy;

class Delete extends AbstractRowStrategy
{

    use GetRowIdByTable;

    public function build()
    {
        $ids = $this->getRowIdsByTable($this->table);
        $where = [];
        $idValues = explode('_', $this->id);
        foreach ($ids as $i => $id) {
            $where[] = '`' . $id . '` = \'' . $idValues[$i] . '\'';
        }

        $sql = 'DELETE FROM `' . $this->table . '` WHERE ' . implode(' AND ', $where);

        $this->sync->addSql($sql);
    }
}