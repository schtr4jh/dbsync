<?php

namespace Dbsync\Service\Strategy;

interface StrategyInterface
{

    public function build();

}