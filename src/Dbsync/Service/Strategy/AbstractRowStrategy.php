<?php

namespace Dbsync\Service\Strategy;

use Dbsync\Service\Sync;

class AbstractRowStrategy implements StrategyInterface
{

    protected $table;

    protected $sync;

    protected $id;

    public function __construct($table, $id, Sync $sync)
    {
        $this->sync = $sync;
        $this->table = $table;
        $this->id = $id;
    }

    public function build()
    {

    }

}