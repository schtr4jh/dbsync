<?php

namespace Dbsync\Service;

trait GetRowIdByTable
{

    protected function replacePrefixes($k) {
        if (substr($k, 0, 4) == '_fw_') {
            $k = substr($k, 4);
        } elseif (substr($k, 0, 5) == '__cm_') {
            $k = substr($k, 5);
        }

        return $k;
    }

    public function getRowIdByTableAndRow($table, $row)
    {
        if (isset($row->id)) {
            $id = $row->id;

        } else if (substr($table, -12) == '_translation') {
            $k = substr($table, 0, -12) . '_id';
            $k = $this->replacePrefixes($k);
            $id = $row->{$k} . '_' . $row->lang_id;

        } else if (strpos($table, '_mtm_')) {
            $mtm = explode('_mtm_', $table);
            $id = [];
            foreach ($mtm as &$m) {
                $m = $this->replacePrefixes($m) . '_id';
                $id[] = $row->{$m};
            }
            $id = implode('_', $id);
        } else {
            $id = '';

        }

        return $id;
    }

    public function getRowIdsByTable($table)
    {
        if (substr($table, -12) == '_translation') {
            $k = substr($table, 0, -12) . '_id';
            if (substr($k, 0, 4) == '_fw_') {
                $k = substr($k, 4);
            } elseif (substr($k, 0, 5) == '__cm_') {
                $k = substr($k, 5);
            }

            return [$k, 'lang_id'];
        } else if (strpos($table, '_mtm_')) {
            $mtm = explode('_mtm_', $table);
            foreach ($mtm as &$m) {
                $m = $this->replacePrefixes($m);
            }
            return $mtm;
        }

        return ['id'];
    }

}